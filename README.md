# 🐍 Kurz Python Jupyter (Ako sa stať programátor, dátový vedec/analytik, herný vývojár)
* Zdrojové kódy ku krúžku/kurzu **Príprava Povolanie a Škola** - Ako sa stať programátor, dátový vedec/analytik, vývojár
* [SPŠ dopravná v Trnave 2021](https://www.spsdtt.sk/)
* Prednášajúci: PhDr. Ing. Mgr. et Mgr. et Mgr. et Mgr. Miroslav Reiter, DiS., MBA, MPA, MSC, DBA, Ing. Paed. IGIP 
* Kontakt: miroslav.reiter@it-academy.sk | miroslav.reiter@fm.uniba.sk 
* Úroveň: začiatočník/mierne pokročilý

## 📈 YouTube video záznamy z cvičení Playlist
[YouTube kanál IT Academy](https://www.youtube.com/watch?v=grSMqteTd40&list=PLIu_ZdHo7Pk-_RYGTUL0NutnK3ljsCHVX)
